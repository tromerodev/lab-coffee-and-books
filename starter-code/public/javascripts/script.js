document.addEventListener('DOMContentLoaded', () => {


  const map = new google.maps.Map(
    document.getElementById('map'), {
      zoom: 11,
    }
  );
  
  loadData(map);
  
  geolocateMe()
    .then(center => map.setCenter(center))
    .catch(e => console.log(e))

}, false);